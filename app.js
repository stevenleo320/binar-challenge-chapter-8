const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const path = require("path");
const session = require("express-session");
const swaggerUI = require("swagger-ui-express");
const OpenApiValidator = require('express-openapi-validator');
const swaggerJSON = require("./api-documentation.json");
const path = require('path');

const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// accept request in form or JSON
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const db = require("./app/models");
db.client.sync();
//NEW
path.resolve('config', 'config.js');
// NEW
app.use(
  OpenApiValidator.middleware({
    apiSpec: path.resolve("./api-documentation.json"),
    validateRequests: true,
    validateResponses: false,
  }),
);

require("./app/routes/player.routes")(app);
// NEW
app.use("/docs", swaggerUI.serve, swaggerUI.setup(swaggerJSON));

app.use((err, req, res, next) => {
  // format error
  res.status(err.status || 500).json({
    message: err.message,
    errors: err.errors,
  });
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
