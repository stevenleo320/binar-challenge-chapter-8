import 'bootstrap/dist/css/bootstrap.min.css';
// import { useEffect, useState } from 'react';
import { useState } from 'react';
// import { Button, Col, Container, FormControl, Row, Form } from 'react-bootstrap';
import { Container, Form, Button, Col, Row } from 'react-bootstrap';

function App() {
  const [players, setPlayer] = useState([]);
  const [newUsername, setUsername] = useState('');
  const [newEmail, setEmail] = useState('');
  const [newPassword, setPassword] = useState('');


  const saveToStorage = (data) => {
    localStorage.setItem("players", JSON.stringify(data));
  }

  const handleSubmitTask = (event) => {
    // Kalau salah satu value username, email or password kosong
    if(!newUsername || !newEmail || !newPassword) return;

    const newPlayer = {
      username: newUsername,
      email: newEmail,
      password: newPassword
    }

    setPlayer([ ...players, newPlayer ]);
    // simpan to local storange
    saveToStorage([ ...players, newPlayer ]);
    // kosongan input apabila sudah berhasil di insert
    setUsername('');
    setEmail('');
    setPassword('');
  }

  const handleUsername = (event) => {
    setUsername(event.target.value);
  }

  const handleEmail = (event) => {
    setEmail(event.target.value);
  }

  const handlePassword = (event) => {
    setPassword(event.target.value);
  }

  const handleUpdateUsername = (idx) => (event) => {
    if (!players[idx].username) return;
    // player index yang diketik akan berubah value nya
    players[idx].username = event.target.value;

    setPlayer([...players]);
    // save ke storange
    saveToStorage([...players]);
  }
  const handleUpdateEmail = (idx) => (event) => {
    if (!players[idx].email) return;
    // player index yang diketik akan berubah value nya
    players[idx].email = event.target.value;

    setPlayer([...players]);
    // save ke storange
    saveToStorage([...players]);
  }

  return (
    <Container>
      <h1>Filter Player</h1>
        <div>
          <table className='table'>
            <thead>
              <tr>
                <th>No.</th>
                <th>Username</th>
                <th>Email</th>
                <th>Password</th>
              </tr>
            </thead>
            <tbody>
            {players.map((player,idx) => (
              <tr>
                <th scope="row">{idx+1}</th>
                <td>{player.username}</td>
                <td>{player.email}</td>
                <td>{player.password}</td>
              </tr>
            ))}
            </tbody>
          </table>
        </div>
      
      <h1>Edit Player</h1>

      {players.map(
        (player, idx) => (
          <Form key={idx} style={{ backgroundColor: "blue", marginBottom: 10 }}>
              <Form.Group as={Row} className="mb-3" style={{ marginLeft: 100 }}>
                <Form.Label>Username</Form.Label>
                <Col xs={11}>
                  <Form.Control value={player.username} onChange={handleUpdateUsername(idx)} style={{ marginBottom: 10 }}/>
                </Col>
              </Form.Group>
              <Form.Group as={Row} className="mb-3" style={{ marginLeft: 100 }}>
                <Form.Label>Email</Form.Label>
                <Col xs={11}>
                  <Form.Control value={player.email} onChange={handleUpdateEmail(idx)} style={{ marginBottom: 10 }}/>
                </Col>
              </Form.Group>
          </Form>
        )
      )}
    
      <h1>Tambah Player</h1>
      <Form>
        <Form.Group className="mb-3">
          <Form.Label>Username</Form.Label>
          <Col xs={10}>
            <Form.Control type="text" value={newUsername} placeholder="Masukkan Username" onChange={handleUsername}/>
          </Col>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Email</Form.Label>
          <Col xs={10}>
            <Form.Control type="email" value={newEmail} placeholder="Masukkan Email" onChange={handleEmail}/>
          </Col>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Password</Form.Label>
          <Col xs={10}>
            <Form.Control type="password" value={newPassword} placeholder="Masukkan Password" onChange={handlePassword}/>
          </Col>
        </Form.Group>
      </Form>

      <Button style={{ marginTop: 10, width: '84%' }} variant="primary" onClick={handleSubmitTask}>Simpan</Button>
      

    </Container>
  );
}

export default App;
